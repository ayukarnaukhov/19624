#Импортирование нужных библиотек
import pandas as pd

#Скачиваение и склеивание таблиц
df1 = pd.read_excel('ploshadi.xls',sheet_name='Sheet1')
df2 = pd.read_excel('2.xlsm',sheet_name='2')
df1.columns = ['адрес', 'УСПД', 'под учётом', 'схема', 'тип',
       'площадь по данным инспекции', 'этажность', 'материал стен',
       'Walls Material', 'год постройки', 'площадь по сайту, S',
       'класс (text)']
df3 = pd.merge(df2,df1,how = 'left',on = 'УСПД')

#Создание новых Excel файлов
df1.to_excel('ploshadi.xlsx',sheet_name = '1')
df2.to_excel('2.xlsx',sheet_name = '1')
df3.to_excel('data_collected_all.xlsx',sheet_name = '1')