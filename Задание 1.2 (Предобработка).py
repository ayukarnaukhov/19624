#Импортирование нужных библиотек
import numpy as np
import pandas as pd

#Скачиваение таблиц
df = pd.read_excel('data_collected_all.xlsx',sheet_name='1')
df.drop('Unnamed: 0',axis = 1,inplace = True)

#Сортировка по УСПД и по Дате
df_1 = df.sort_values(['УСПД','Дата'],ascending=[True,True])
df_1 = df_1.reset_index()
df_1.drop('index',axis = 1,inplace = True)

#Удаление колонки с адресами
df_1_1 = df_1.copy() #Создание второй таблицы где будут адреса
df_1.drop('адрес',axis = 1,inplace = True)

#Прописывание схем Отоплению
df_1.iloc[df_1[df_1['под учётом'] == 'Отопление']['схема'].index,13] = 'закрытая' #Без адресов
df_1_1.iloc[df_1_1[df_1_1['под учётом'] == 'Отопление']['схема'].index,14] = 'закрытая' #С адресами

#Создание таблицы дополненной
df_1.to_excel('data_collected_all_schem.xlsx',sheet_name = '1') #Без адресов
df_1_1.to_excel('data_collected_all_schem_with_adress.xlsx',sheet_name = '1') #С адресами

#Удаление пропущенных строк
df_2 = df_1.copy()
df_2_1 = df_1_1.copy()
df_2.dropna(axis = 0,inplace = True)
df_2_1.dropna(axis = 0,inplace = True)

#Добавление столбца с улицами
df_2_1['Улицы'] = df_2_1['адрес'].apply(lambda x: x.split(' ,')[0])
df_2['Улицы'] = df_2_1['Улицы']

#Создание таблицы без Пропусков
df_2.to_excel('data_collected_without_nan.xlsx',sheet_name = '1')
df_2_1.to_excel('data_collected_without_nan_with_adress.xlsx',sheet_name = '1')