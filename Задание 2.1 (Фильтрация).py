#Импортирование нужных библиотек
import pandas as pd
import numpy as np

#Скачиваение таблиц
df = pd.read_excel('data_collected_without_nan.xlsx',sheet_name='1')
df_1 = pd.read_excel('data_collected_without_nan_with_adress.xlsx',sheet_name='1')

df_1_1 = df.copy()
df_1_2 = df_1.copy()

df_1_1.drop('Unnamed: 0',axis = 1,inplace = True)
df_1_2.drop('Unnamed: 0',axis = 1,inplace = True)

#Создание фильтрующей функции по квартилям
def data_filt_IQR(y,_25,_75,IQR):
    if (y<_75+3*IQR) and  (y>_25-3*IQR):
        return True

#Создание таблиц с отфлитрованными значениями
df_1_1_filtered = pd.DataFrame(columns = df_1_1.columns)
df_1_2_filtered = pd.DataFrame(columns = df_1_2.columns)

#Фильтрация 
for i in df_1_1['УСПД'].unique():
    
    filtered_data = []
    
    df_control = df_1_1[df_1_1['УСПД'] == i].copy()
    
    _25 = df_control['Q, ГКалл'].quantile(.25)
    _75 = df_control['Q, ГКалл'].quantile(.75)
    IQR = _75 - _25
    
    
    for j in df_control['Q, ГКалл']:
        if data_filt_IQR(j,_25,_75,IQR):
            filtered_data.append(list(df_control[df_control['Q, ГКалл'] == j]['Дата'])[0])
    
    df_1_1_filtered = pd.concat([df_1_1_filtered,df_control[df_control['Дата'].apply(lambda x: x in filtered_data)]])

for i in df_1_2['УСПД'].unique():
    
    filtered_data = []
    
    df_control = df_1_2[df_1_2['УСПД'] == i].copy()
    
    _25 = df_control['Q, ГКалл'].quantile(.25)
    _75 = df_control['Q, ГКалл'].quantile(.75)
    IQR = _75 - _25
    
    
    for j in df_control['Q, ГКалл']:
        if data_filt_IQR(j,_25,_75,IQR):
            filtered_data.append(list(df_control[df_control['Q, ГКалл'] == j]['Дата'])[0])
    
    df_1_2_filtered = pd.concat([df_1_2_filtered,df_control[df_control['Дата'].apply(lambda x: x in filtered_data)]])
    
#Создание новой таблицы Excel с отфилтрованными значениями
df_1_1_filtered = df_1_1_filtered.reset_index().drop('index',axis = 1)
df_1_2_filtered = df_1_2_filtered.reset_index().drop('index',axis = 1)

df_1_1_filtered.to_excel('data_filtered_with_boxplot.xlsx',sheet_name = '1')
df_1_2_filtered.to_excel('data_filtered_with_boxplot_with_adress.xlsx',sheet_name = '1')