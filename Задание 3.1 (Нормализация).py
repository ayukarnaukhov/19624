#Импортирование нужных библиотек
import pandas as pd
import seaborn as sns
import numpy as np
import scipy

#Скачивание таблиц
df = pd.read_excel('data_filtered_with_boxplot.xlsx',sheet_name='1')
df_1 = pd.read_excel('data_filtered_with_boxplot_with_adress.xlsx',sheet_name='1')

df_1_1 = df.copy()
df_1_2 = df_1.copy()

df_1_1.drop('Unnamed: 0',axis = 1,inplace = True)
df_1_2.drop('Unnamed: 0',axis = 1,inplace = True)

#Создание положительной таблицы
df_1_1 = df_1_1[df_1_1['Q, ГКалл'] > 0]
df_1_1.to_excel('data_filtered_with_boxplot_pos.xlsx',sheet_name = '1')

df_1_2 = df_1_2[df_1_2['Q, ГКалл'] > 0]
df_1_2.to_excel('data_filtered_with_boxplot_pos_with_adress.xlsx',sheet_name = '1')

#Пеобразование данных в подобие нормального распределения
df_1_1_1 = df_1_1.copy()
lmbda = scipy.stats.boxcox_normmax(df_1_1_1['Q, ГКалл'])
df_1_1_1['Q, ГКалл'] = scipy.stats.boxcox(df_1_1_1['Q, ГКалл'],lmbda = lmbda)

df_1_2_1 = df_1_2.copy()
lmbda = scipy.stats.boxcox_normmax(df_1_2_1['Q, ГКалл'])
df_1_2_1['Q, ГКалл'] = scipy.stats.boxcox(df_1_2_1['Q, ГКалл'],lmbda = lmbda)

#Содание таблицы с преобразованными данными
df_1_1_1.to_excel('data_norm.xlsx',sheet_name = '1')
df_1_2_1.to_excel('data_norm_with_adress.xlsx',sheet_name = '1')