#Скачивание нужных библиотек
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
import statsmodels
import scipy
import math

import statsmodels.api as sm
from statsmodels.formula.api import ols

#Скачивание таблицы
df = pd.read_excel('data_filtered_with_boxplot_pos.xlsx',sheet_name = '1')
df.drop('Unnamed: 0',axis = 1,inplace = True)
df.columns = ['Дата', 'М1, т', 'М2, т', 'ΔМ, т', 'Т1, °C', 'Т2, °C', 'ΔТ, °C',
       'Q_ГКалл', 'BHP, ч', 'BOC, ч', 'УСПД', 'ГГГГММ', 'под_учётом', 'схема',
       'тип', 'площадь_по_данным_инспекции', 'этажность', 'материал_стен',
       'Walls_Material', 'год_постройки', 'площадь_по_сайту, S',
       'класс (text)']
df_1 = df.copy()

#Номинативные параметры
param = ['под_учётом', 'схема', 'тип','этажность', 'материал_стен', 'год_постройки']

#Параметрический метод
F_param = dict()
for i in param[:]:
    data = []
    for j in df_1[i].unique():
        data.append(df_1[df_1[i] == j]['Q_ГКалл'])
    F_param[i] = scipy.stats.f_oneway(*data)[:]
print('F_param\n',F_param,'\n\n\n')
    
#Непараметрический метод
H_param = dict()
for i in param[:]:
    groups = {}
    for grp in df_1[i].unique():
        groups[grp] = df_1['Q_ГКалл'][df_1[i]==grp].values
    args = groups.values()
    H_param[i] = scipy.stats.kruskal(*args)
print('H_param\n',H_param,'\n\n\n')

#Многофакторный ANOVA параметрический
formula = 'Q_ГКалл ~ C(этажность,Sum) + C(материал_стен,Sum) + C(этажность,Sum):C(материал_стен,Sum)'
model = ols(formula, df_1).fit()
aov_table = statsmodels.stats.anova.anova_lm(model, typ=1)
print(aov_table,'\n\n\n')

#Сравнение групп в факторах
for i in param[2:]:
  mc = statsmodels.stats.multicomp.MultiComparison(list(zip(df_1['Q_ГКалл'])),list(zip(df_1['{}'.format(i)])))
  result = mc.tukeyhsd()
  print(result)
  print(mc.groupsunique,'\n\n\n')