import pandas as pd
import numpy as np

df1 = pd.read_excel('data_filtered_with_boxplot_pos_with_adress.xlsx',sheet_name='1',index_col='Unnamed: 0')
df2 = pd.read_excel('data_filtered_with_boxplot_pos.xlsx',sheet_name='1',index_col='Unnamed: 0')
df3 = pd.read_excel('data_filtered_with_boxplot_with_adress.xlsx',sheet_name='1',index_col='Unnamed: 0')
df4 = pd.read_excel('data_filtered_with_boxplot.xlsx',sheet_name='1',index_col='Unnamed: 0')
df5 = pd.read_excel('data_norm_with_adress.xlsx',sheet_name='1',index_col='Unnamed: 0')
df6 = pd.read_excel('data_norm.xlsx',sheet_name='1',index_col='Unnamed: 0')

df7 = pd.read_excel('TOR_t.xlsx',sheet_name='Лист1')

df7.columns = ['Дата', 'temp']

df1 = pd.merge(df1,df7,how='inner',on='Дата')
df2 = pd.merge(df2,df7,how='inner',on='Дата')
df3 = pd.merge(df3,df7,how='inner',on='Дата')
df4 = pd.merge(df4,df7,how='inner',on='Дата')
df5 = pd.merge(df5,df7,how='inner',on='Дата')
df6 = pd.merge(df6,df7,how='inner',on='Дата')

df1.to_excel('data_filtered_with_boxplot_pos_with_adress.xlsx',sheet_name='1')
df2.to_excel('data_filtered_with_boxplot_pos.xlsx',sheet_name='1')
df3.to_excel('data_filtered_with_boxplot_with_adress.xlsx',sheet_name='1')
df4.to_excel('data_filtered_with_boxplot.xlsx',sheet_name='1')
df5.to_excel('data_norm_with_adress.xlsx',sheet_name='1')
df6.to_excel('data_norm.xlsx',sheet_name='1')