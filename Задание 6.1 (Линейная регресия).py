import pandas as pd
import numpy as np
from sklearn import linear_model
from statsmodels.tools.eval_measures import rmse
import pickle

# Скачиваем нужную таблицу
df = pd.read_excel('data_filtered_with_boxplot.xlsx',index_col='Дата',sheet_name='1')
df = df.drop('Unnamed: 0',axis = 1)

# Создаем копию для обучения
df1 = df.copy()
df1 = df1[['Дата',
          'М1, т', 
           'М2, т', 
           'ΔМ, т',
           'Т1, °C', 
           'Т2, °C', 
           'ΔТ, °C', 
           'Q, ГКалл',
       'BHP, ч', 
       'BOC, ч', 
       'УСПД', 
       'под учётом', 
       'схема', 
       'площадь по данным инспекции', 
       'этажность', 
       'материал стен',
       'год постройки', 
       'площадь по сайту, S',
       'temp']]

# Вспомогательная функция для перевода номинативные данных в цифры
def word2num(x):
  a = dict()
  o = 1
  for i in x.unique():
    a[i] = o
    o +=1
  return a

# Столбцы которые для перевода в числа
columns = ['под учётом','схема','материал стен']

# Перевод в числа и добавление в таблицу
for i in columns:
  dict_col = word2num(df1[i])
  df1[i + ' (индекс)'] = df1[i].apply(lambda x: dict_col[x])

# Столбцы для обучения
columns_to_use = [
       'площадь по данным инспекции',
       'этажность', 
       'год постройки', 
       'площадь по сайту, S',
       'temp',
       'под учётом (индекс)',
       'схема (индекс)',
       'материал стен (индекс)'
       ]

# Создание модели и занесение предсказанных значений в таблицу
linear_model_model = linear_model.LinearRegression()
linear_model_model.fit(df1[columns_to_use],df1['Q, ГКалл'])
df1['pred'] = linear_model_model.predict(df1[columns_to_use])

# Вывод среднеквадратичной ошибки
print(rmse(df1['Q, ГКалл'],df1['pred']))

# Сохранение модели
file_name = 'Linear Regression Model.data'
with open(file_name,'wb') as f:
    pickle.dump(linear_model_model,f)






