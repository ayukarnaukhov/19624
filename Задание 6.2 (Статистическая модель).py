import pandas as pd
import numpy as np
from statsmodels.tools.eval_measures import rmse
import pickle
from pmdarima import auto_arima
from statsmodels.tsa.statespace.sarimax import SARIMAX
import datetime

df = pd.read_excel('data_filtered_with_boxplot.xlsx',sheet_name='1')
df = df.drop('Unnamed: 0',axis = 1)

# Создаем копию для обучения
df1 = df.copy()
df1 = df1[['Дата',
          'М1, т', 
           'М2, т', 
           'ΔМ, т',
           'Т1, °C', 
           'Т2, °C', 
           'ΔТ, °C', 
           'Q, ГКалл',
       'BHP, ч', 
       'BOC, ч', 
       'УСПД', 
       'под учётом', 
       'схема', 
       'площадь по данным инспекции', 
       'этажность', 
       'материал стен',
       'год постройки', 
       'площадь по сайту, S',
       'temp']]

columns = ['под учётом','схема','материал стен']

df1 = df1.join(pd.get_dummies(df[columns]))
columns_to_use_2 = [
       'этажность', 
       'под учётом_Отопление',
       'под учётом_Отопление+ГВС', 
       'схема_закрытая',
        'схема_открытая',
       'материал стен_блок', 
       'материал стен_дерево',
        'материал стен_кирпич',
       'материал стен_монолит',
        'материал стен_панель'
       ]

df_for_SARIMAX = df1[df1['УСПД'] == 3]
date_array = pd.date_range(df_for_SARIMAX['Дата'].iloc[0],df_for_SARIMAX['Дата'].iloc[0] + datetime.timedelta(days = len(df_for_SARIMAX)-1),freq = 'D')
df_for_SARIMAX.index = date_array

train = df_for_SARIMAX.iloc[:(len(df_for_SARIMAX)-30)]
test = df_for_SARIMAX.iloc[(len(df_for_SARIMAX)-30):]

auto_arima_summary = auto_arima(df_for_SARIMAX['Q, ГКалл'],seasonal=True,exogenous=df_for_SARIMAX[columns_to_use_2],m = 60)
order = auto_arima_summary.order
seasonal_order = auto_arima_summary.seasonal_order
model_sarimax = SARIMAX(train['Q, ГКалл'],order=order,seasonal_order=seasonal_order,exog=train[columns_to_use_2],enforce_invertibility=False)
result = model_sarimax.fit()

start = len(train)
end = start + len(test) - 1

pred = result.predict(start,end,exog = test[columns_to_use_2])

file_name = 'SARIMAX Model.data'
with open(file_name,'wb') as f:
    pickle.dump(result,f)