import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import pickle

df = pd.read_excel('data_filtered_with_boxplot.xlsx',sheet_name='1',index_col=0)
df1 = df[['Дата', 'М1, т', 'М2, т', 'ΔМ, т', 'Т1, °C', 'Т2, °C', 'ΔТ, °C',
       'Q, ГКалл', 'BHP, ч', 'BOC, ч', 'УСПД','под учётом', 'схема',
       'площадь по данным инспекции', 'этажность', 'материал стен',
        'год постройки', 'площадь по сайту, S','temp']].copy()
df1 = pd.get_dummies(df1,columns=['под учётом','схема','материал стен'])

train_col = ['Q, ГКалл','площадь по данным инспекции',
             'этажность','площадь по сайту, S','под учётом_Отопление',
             'под учётом_Отопление+ГВС','схема_закрытая','схема_открытая','материал стен_блок',
             'материал стен_дерево','материал стен_кирпич','материал стен_монолит','материал стен_панель']

df1_train = df1[df1['УСПД'] == 1].reset_index().drop(columns = 'index').iloc[:round(len(df1[df1['УСПД'] == 1])*0.7)]
df1_test = df1[df1['УСПД'] == 1].reset_index().drop(columns = 'index').iloc[round(len(df1[df1['УСПД'] == 1])*0.7):]

train_data = df1_train[train_col]
test_data = df1_test[train_col]

scaler = MinMaxScaler()
train_data = scaler.fit_transform(train_data)

X_train = []
Y_train = []

for i in range(60,train_data.shape[0]):
  X_train.append(train_data[i-60:i])
  Y_train.append(train_data[i,0])

X_train,Y_train = np.array(X_train),np.array(Y_train)

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense,LSTM,Dropout

regressior = Sequential()
regressior.add(LSTM(units=50,activation='relu',return_sequences=True, input_shape = (X_train.shape[1],X_train.shape[2])))
regressior.add(Dropout(0.2))
regressior.add(LSTM(units=60,activation='relu',return_sequences=True ))
regressior.add(Dropout(0.3))
regressior.add(LSTM(units=80,activation='relu',return_sequences=True))
regressior.add(Dropout(0.4))
regressior.add(LSTM(units=120,activation='relu'))
regressior.add(Dropout(0.5))
regressior.add(Dense(units = 1))
regressior.compile(optimizer='adam',loss = 'mean_squared_error')
regressior.fit(X_train,Y_train,epochs=20,batch_size=500)

past_60_days = df1_train[train_col].tail(60)
df_2 = pd.concat([past_60_days,df1_test[train_col]],ignore_index=True)
inputs = scaler.transform(df_2)

x_test = []
y_test = []

for i in range(60,inputs.shape[0]):
  x_test.append(inputs[i-60:i])
  y_test.append(inputs[i,0])

x_test,y_test = np.array(x_test),np.array(y_test)
y_pred = regressior.predict(x_test)
y_pred *= 1/scaler.scale_[0]
y_test *= 1/scaler.scale_[0]

df_pred = pd.DataFrame(columns = ['Тестовые значения','Предсказанные значения'])
df_pred['Тестовые значения'] = y_test.reshape((222))
df_pred['Предсказанные значения'] = y_pred.reshape((222))

file_name = 'RNN.data'
weigh= regressior.get_weights();

regressior.save('RNN.h5')
